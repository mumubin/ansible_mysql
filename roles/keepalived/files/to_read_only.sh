#!/bin/bash
echo "set read_only=1" >> {{basedir}}/shbin/m.log
echo "set super_read_only=1" >> {{basedir}}/shbin/m.log


{{basedir}}/bin/mysql  -u{{login_user}} -p{{login_password}} -e "set global read_only=1"
{{basedir}}/bin/mysql -u{{login_user}} -p{{login_password}} -e "set global super_read_only=1"
{{basedir}}/bin/mysql -u{{login_user}} -p{{login_password}} -e "show variables like '%read_only%'" >> {{basedir}}/shbin/m.log