# ansible_mysql_keepalived

## Description
This playbook is for mysql install with master to master semisync and keepalived in offline env.
If u have Internet access, please refer to [https://github.com/geerlingguy/ansible-role-mysql](https://github.com/geerlingguy/ansible-role-mysql).

For Now just support Centos7, If u have other requires, please submit pull request.
Centos7 Only

## Usage
Master to Master semi sync deployment 
```shell
 ansible-playbook -i host.yaml site.yaml  -e @vars/variables.yaml
```

## Test 
```shell
python3 -m pip install --user "molecule[docker,lint]"
cd roles/test
molecule converge
```